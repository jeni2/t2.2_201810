package model.data_structures;

import java.util.Date;

import com.sun.javafx.scene.paint.GradientUtils.Point;

public class Node <E>{


	//	//
	//	private String company;
	//	private int  census_tract ;
	//	private double centroid_latitude;
	//	private Point centroid_location;
	//	private double centroid_longitude;
	//	private int community_area;
	//	private int  extras;
	//	private int fare;
	//	private String payment_type;
	//	private int pickup_census_tract;
	//	private double pickup_centroid_latitude;
	//	private Point pickup_centroid_location;
	//	private double pickup_centroid_longitude;
	//	private int pickup_community_area;
	//	private String taxi_id;
	//	private int trips;
	//	private int tolls;
	//	private Date trip_end_timestamp;
	//	private String trip_id;
	//	private double trip_miles;
	//	private double trip_seconds;
	//	private Date trip_start_timestamp;
	//	private double trip_total;
	//	
	//	public String getCompany() {
	//		return company;
	//	}
	//	public void setCompany(String company) {
	//		this.company = company;
	//	}
	//	public double getCensus_tract() {
	//		return census_tract;
	//	}
	//	public void setCensus_tract(int census_tract) {
	//		this.census_tract = census_tract;
	//	}
	//	public double getCentroid_latitude() {
	//		return centroid_latitude;
	//	}
	//	public void setCentroid_latitude(double centroid_latitude) {
	//		this.centroid_latitude = centroid_latitude;
	//	}
	//	public Point getCentroid_location() {
	//		return centroid_location;
	//	}
	//	public void setCentroid_location(Point centroid_location) {
	//		this.centroid_location = centroid_location;
	//	}
	//	public double getCentroid_longitude() {
	//		return centroid_longitude;
	//	}
	//	public void setCentroid_longitude(double centroid_longitude) {
	//		this.centroid_longitude = centroid_longitude;
	//	}
	//	public int getCommunity_area() {
	//		return community_area;
	//	}
	//	public void setCommunity_area(int community_area) {
	//		this.community_area = community_area;
	//	}
	//	public int getExtras() {
	//		return extras;
	//	}
	//	public void setExtras(int extras) {
	//		this.extras = extras;
	//	}
	//	public int getFare() {
	//		return fare;
	//	}
	//	public void setFare(int fare) {
	//		this.fare = fare;
	//	}
	//	public String getPayment_type() {
	//		return payment_type;
	//	}
	//	public void setPayment_type(String payment_type) {
	//		this.payment_type = payment_type;
	//	}
	//	public int getPickup_census_tract() {
	//		return pickup_census_tract;
	//	}
	//	public void setPickup_census_tract(int pickup_census_tract) {
	//		this.pickup_census_tract = pickup_census_tract;
	//	}
	//	public double getPickup_centroid_latitude() {
	//		return pickup_centroid_latitude;
	//	}
	//	public void setPickup_centroid_latitude(double pickup_centroid_latitude) {
	//		this.pickup_centroid_latitude = pickup_centroid_latitude;
	//	}
	//	public Point getPickup_centroid_location() {
	//		return pickup_centroid_location;
	//	}
	//	public void setPickup_centroid_location(Point pickup_centroid_location) {
	//		this.pickup_centroid_location = pickup_centroid_location;
	//	}
	//	public double getPickup_centroid_longitude() {
	//		return pickup_centroid_longitude;
	//	}
	//	public void setPickup_centroid_longitude(double pickup_centroid_longitude) {
	//		this.pickup_centroid_longitude = pickup_centroid_longitude;
	//	}
	//	public int getPickup_community_area() {
	//		return pickup_community_area;
	//	}
	//	public void setPickup_community_area(int pickup_community_area) {
	//		this.pickup_community_area = pickup_community_area;
	//	}
	//	public String getTaxi_id() {
	//		return taxi_id;
	//	}
	//	public void setTaxi_id(String taxi_id) {
	//		this.taxi_id = taxi_id;
	//	}
	//	public int getTrips() {
	//		return trips;
	//	}
	//	public void setTrips(int trips) {
	//		this.trips = trips;
	//	}
	//	public int getTolls() {
	//		return tolls;
	//	}
	//	public void setTolls(int tolls) {
	//		this.tolls = tolls;
	//	}
	//	public Date getTrip_end_timestamp() {
	//		return trip_end_timestamp;
	//	}
	//	public void setTrip_end_timestamp(Date trip_end_timestamp) {
	//		this.trip_end_timestamp = trip_end_timestamp;
	//	}
	//	public String getTrip_id() {
	//		return trip_id;
	//	}
	//	public void setTrip_id(String trip_id) {
	//		this.trip_id = trip_id;
	//	}
	//	public double getTrip_miles() {
	//		return trip_miles;
	//	}
	//	public void setTrip_miles(double trip_miles) {
	//		this.trip_miles = trip_miles;
	//	}
	//	public double getTrip_seconds() {
	//		return trip_seconds;
	//	}
	//	public void setTrip_seconds(double trip_seconds) {
	//		this.trip_seconds = trip_seconds;
	//	}
	//	public Date getTrip_start_timestamp() {
	//		return trip_start_timestamp;
	//	}
	//	public void setTrip_start_timestamp(Date trip_start_timestamp) {
	//		this.trip_start_timestamp = trip_start_timestamp;
	//	}
	//	public double getTrip_total() {
	//		return trip_total;
	//	}
	//	public void setTrip_total(double trip_total) {
	//		this.trip_total = trip_total;
	//	}
	//	


	private Node<E> next;
	private Node<E> previous;
	
	private E item;

	public Node (E item) {
		next = null;
		setPrevious(null);
		this.item = item;
	}

	public Node<E> getNext() {
		return next;
	}

	public void setNextNode ( Node<E> next) {
		this.next = next;
	}

	public E getItem(){
		return item;
	}

	public void setItem (E item) {
		this.item = item;
	}

	public boolean hasNext() {
		// TODO Auto-generated method stub
		return false;
	}

	public Node<E> getPrevious() {
		return previous;
	}

	public void setPrevious(Node<E> previous) {
		this.previous = previous;
	}



}
